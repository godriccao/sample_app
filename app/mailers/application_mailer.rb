class ApplicationMailer < ActionMailer::Base
  default from: "noreply@hogworts.org"
  layout 'mailer'
end
