require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "Rubeus Hagrid", 
                     email: "r.giant.hagrid@hogwarts.org",
                     password: "harrypotter", password_confirmation: "harrypotter")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "name should be within 50 chars" do
    @user.name = "H" * 51
    assert_not @user.valid?
  end
  
  test "email addresses should be within 255 chars" do
    @user.name = "H" * (256 - 13) + "@hogwarts.org"
    assert_not @user.valid?
  end
  
  test "email addresses validation should accept valid addresses" do
    valid_addresses = %w[r.giant.hagrid@hogwarts.org
                         h_potter@order.of.phoenix.org
                         Ron+Hermy@weasly.family.home
                         T-M-Riddle.voldemort@d.e.com]
    valid_addresses.each do |valid_address|
        @user.email = valid_address
        assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email addresses validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com
                           draco.malfoy@de..com lucius.malfoy@..de.com
                           Ron+Hermy@weasly.family.homes]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "email address should be unique" do
    dup_user = @user.dup
    dup_user.email.upcase!
    @user.save
    assert_not dup_user.valid?
  end
  
  test "email should be saved as lower-case" do
    mixed_case_email = "R.G.Hagrid@Hogwarts.ORG"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password should have a minimum length of 6" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user wiht nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    dumb  = users(:dumb)
    harry = users(:harry)
    #assert_not harry.following?(dumb)
    harry.follow(dumb)
    assert harry.following?(dumb)
    assert dumb.followers.include?(harry)
    harry.unfollow(dumb)
    assert_not harry.following?(dumb)
  end
  
  test "feed should have the right posts" do
    dumb  = users(:dumb)
    snape = users(:snape)
    black = users(:black)
    # Posts from followed user
    dumb.microposts.each do |post_following|
      assert snape.feed.include?(post_following)
    end
    # Posts from self
    dumb.microposts.each do |post_self|
      assert dumb.feed.include?(post_self)
    end
    # Posts from unfollowed user
    black.microposts.each do |post_unfollowed|
      assert_not snape.feed.include?(post_unfollowed)
    end
  end
end
