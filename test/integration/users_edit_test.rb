require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:dumb)
  end

  test "unsuccessful edit" do
    log_in_as(@user, password: "phoenix")
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  "",
                                    email: "old@elder",
                                    password:              "fox",
                                    password_confirmation: "finix" }
    assert_template 'users/edit'
  end
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user, password: "phoenix")
    assert_redirected_to edit_user_path(@user)
    assert_equal session[:forwarding_url], nil
    # assert_template 'users/edit'
    name  = "A. Dumbledore"
    email = "a.dumb@hogworts.org"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name,  name
    assert_equal @user.email, email
  end
end
