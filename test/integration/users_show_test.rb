require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user             = users(:dumb)
    @activated_user   = users(:snape)
    @inactivated_user = users(:harry)
  end
  
  test "show activated user" do
    log_in_as(@user, password: "phoenix")
    get user_path(@activated_user)
    assert_template 'users/show'
    assert_match /#{@activated_user.name}/, response.body
  end
 
  test "show inactivated user" do
    log_in_as(@user, password: "phoenix")
    get user_path(@inactivated_user)
    assert_redirected_to root_url
  end
end
